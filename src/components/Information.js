import React, { Component } from 'react';
import Mise from "./Mise";
import Resume from "./Resume";

class Information extends Component {

    constructor(props){
        super(props);
        this.state = {
            montant: ''
        }
    }

    render() {
        return (
            <div className="col-12 informations">
                <div className="row">
                    <div className="col-md-8 d-none d-sm-block d-sm-none d-md-block">
                        <Resume/>
                    </div>
                    <div className="col-md-4 col-xs-12">
                        <Mise montant={this.props.montant}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Information;
