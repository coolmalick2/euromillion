import React, { Component } from 'react';

class Boule extends Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.checkPrice = this.checkPrice.bind(this);
        this.updateView = this.updateView.bind(this);
        global.compteur = 0;
        global.complete = '';
        this.state = {
            active: false,
        };
    }


    componentDidUpdate() {
        this.updateView();
    }


    updateView = () => {
        let boules = parseInt(localStorage.getItem('boules'));
        let etoiles = parseInt(localStorage.getItem('etoiles'));

        if((boules === 6 && etoiles >= 7) || (boules === 10) || (boules === 9 && etoiles === 3) || (boules === 8 && etoiles === 4) || (boules === 7 && etoiles === 5)){
            global.complete = 'bouleDisabled';
        }else{
            global.complete = '';
        }
    };

    increment = () => {
        global.compteur++;
    };

    decrement = () => {
        global.compteur--;
    };

    checkPrice = () => {
        let values = JSON.parse(localStorage.getItem('jsonArray'));
        let boules = parseInt(localStorage.getItem('boules'));
        let etoiles = parseInt(localStorage.getItem('etoiles'));

        if(boules >= 5 && etoiles >= 2){
            for(let i = 0; i < values.length; i++){
                if(values[i].pattern[0] === boules && values[i].pattern[1] === etoiles){
                    let montant = values[i].cost['value'] / 100;
                    montant = montant +' €';
                    this.props.getInput(montant);
                }
            }
        }else{
            this.props.getInput(0+' €');
        }
    };

    handleClick = (e) => {
        let hisClass = e.target.className;
        if(!hisClass.includes('bouleDisabled')){
            const currentState = this.state.active;
            currentState ? this.decrement() : this.increment();
            this.setState({ active: !currentState });
        }
        localStorage.removeItem('boules');
        localStorage.setItem('boules', global.compteur);
        this.checkPrice();
        this.updateView();
    };

    render() {
        return (
            <div className={this.state.active ? 'm-2 numberCellSelected text-center d-flex justify-content-center align-items-center': 'm-2 numberCell '+ global.complete +' text-center d-flex justify-content-center align-items-center'} onClick={this.handleClick}>
                { this.props.Value }
            </div>
        );
    }
}

export default Boule;
