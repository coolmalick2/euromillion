import React, { Component } from 'react';
import Etoile from "./Etoile";

class Etoiles extends Component {

    constructor(props){
        super(props);
        this.state = {
            input:'',
            triger: ''
        };
        this.handleInput=this.handleInput.bind(this);
    }

    componentDidUpdate(oldProps) {
        const newProps = this.props;
        if(oldProps.triger !== newProps.triger) {
            console.log("Trigger");
            this.setState({triger: newProps.triger })
        }
    }

    handleInput(value){
        this.setState({
            input:value
        });
        this.props.getInput2(value);
    }

    componentDidMount() {
        localStorage.setItem('etoiles', 0);
        localStorage.removeItem('total');
    }

    createStars = () => {
        let etoiles = [];

        for (let i = 1; i <= 12; i++){
            etoiles.push(<Etoile key={i} Value={i}  getInput={this.handleInput}/>)
        }
        return etoiles;
    };

    render() {
        return (
            <div className="d-flex stars-flex align-content-start flex-wrap">
                { this.createStars() }
            </div>
        );
    }
}

export default Etoiles;
