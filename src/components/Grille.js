import React, { Component } from 'react';
import Boules from "./Boules";
import Etoiles from "./Etoiles";

class Grille extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoaded: false,
            items: {},
        };
        this.handleInput2=this.handleInput2.bind(this);
    }

    handleInput2(value){
        this.setState({
            input:value
        });
        this.props.getInput3(value);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <div className="col-8">
                        <Boules triger={this.state.input} getInput2={this.handleInput2}/>
                    </div>
                    <div className="col-4">
                        <Etoiles triger={this.state.input}  getInput2={this.handleInput2}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Grille;
