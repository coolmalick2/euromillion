import React, { Component } from 'react';
import './etoile.css'

class Etoile extends Component {

    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.checkPrice = this.checkPrice.bind(this);
        this.updateView = this.updateView.bind(this);
        global.complete2 = 0;
        global.compteurEtoile = 0;
        this.state = {
            active: false,
        };
    }

    componentDidUpdate() {
        this.updateView();
    }

    updateView = () => {
        let boules = parseInt(localStorage.getItem('boules'));
        let etoiles = parseInt(localStorage.getItem('etoiles'));

        if((boules === 7 && etoiles === 6) || (boules === 8 && etoiles === 4) || (boules === 9 && etoiles === 3) || (boules === 10 && etoiles === 2)){
            global.complete2 = 'etoileDisabled';
        }else{
            global.complete2 = '';
        }
    };

    checkPrice = () => {
        let values = JSON.parse(localStorage.getItem('jsonArray'));
        let boules = parseInt(localStorage.getItem('boules'));
        let etoiles = parseInt(localStorage.getItem('etoiles'));
        if(boules >= 5 && etoiles >= 2){
            for(let i = 0; i < values.length; i++){
                if(values[i].pattern[0] === boules && values[i].pattern[1] === etoiles){
                    let montant = values[i].cost['value'] / 100;
                    montant = montant +' €';
                    this.props.getInput(montant);
                }
            }
        }else{
            this.props.getInput(0+' €');
        }
    };

    increment = () => {
        global.compteurEtoile++;
    };

    decrement = () => {
        global.compteurEtoile--;
    };

    handleClick = (e) => {

        let hisClass = e.target.className;
        if(!hisClass.includes('etoileDisabled')){
            const currentState = this.state.active;
            currentState ? this.decrement() : this.increment();
            this.setState({ active: !currentState });
        }


        const currentState = this.state.active;
        currentState ? this.decrement() : this.increment();

        this.setState({ active: !currentState });

        localStorage.removeItem('etoiles');
        localStorage.setItem('etoiles', global.compteurEtoile);
        this.checkPrice();
        this.updateView();
    };

    render() {
        return (
            <div className={this.state.active ? 'm-2 etoileSelected text-center d-flex justify-content-center align-items-center': 'm-2 etoileNotSelected '+ global.complete2 +' text-center d-flex justify-content-center align-items-center'} onClick={this.handleClick}>
                <i className="fas fa-star"></i>
                <span className="textInside">
                    { this.props.Value }
                </span>
            </div>
        );
    }
}

export default Etoile;
