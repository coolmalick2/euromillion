import React, { Component } from 'react';
import Boule from "./Boule";

class Boules extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoaded: false,
            input:'',
            triger: ''
        };
        this.handleInput=this.handleInput.bind(this);
    }

    componentDidUpdate(oldProps) {
        const newProps = this.props;
        if(oldProps.triger !== newProps.triger) {
            console.log("Trigger");
            this.setState({triger: newProps.triger })
        }
    }

    handleInput(value){
        this.setState({
            input:value
        });
        this.props.getInput2(value);
    }

    componentDidMount() {
        localStorage.clear();
        localStorage.setItem('boules', 0);
        localStorage.removeItem('total');
        fetch("https://api.myjson.com/bins/6ss0k")
        //fetch("https://www.fdj.fr/apigw/rtg/rest/euromillions", {'mode': 'no-cors'}
        .then(results =>  results.json())
        .then(multiples => localStorage.setItem("jsonArray",  JSON.stringify(multiples.multiples)));
    }

    createBowls = () => {
        let boules = [];

        for (let i = 1; i <= 50; i++){
            boules.push(<Boule key={i} Value={i} getInput={this.handleInput} triger={this.state.triger}/>)
        }
        return boules;
    };

    render() {
        return (
            <div className="d-flex align-content-start flex-wrap">
                { this.createBowls() }
            </div>
        );
    }
}

export default Boules;
