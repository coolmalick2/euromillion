import React, { Component } from 'react';

class Mise extends Component {

    constructor(props){
        super(props);
        this.state = {
            montant: ''
        }
    }
    render() {
        return (
            <div className="miseTotaleContainer">
                <div className="miseTotale">
                    Mise Totale { this.props.montant }
                </div>
            </div>
        );
    }
}

export default Mise;
