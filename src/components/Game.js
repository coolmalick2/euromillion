import React, { Component } from 'react';
import Information from "./Information";
import Grille from "./Grille";
import './game.css';

class Game extends Component {
    constructor(props){
        super(props);
        this.state = {
            input:'0 €'
        };
        this.handleInput3=this.handleInput3.bind(this);
    }

    handleInput3(value){
        this.setState({
            input:value
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Information montant={this.state.input}/>
                </div>
                <div className="row">
                    <Grille getInput3={this.handleInput3}/>
                </div>
            </div>
        );
    }
}

export default Game;
